﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using proyectosig.Models;

namespace proyectosig.Controllers
{
    public class LoginAccountController : Controller
    {

        SqlConnection con = new SqlConnection();
        SqlCommand com = new SqlCommand();
        SqlDataReader dr;
        DataProjectDataContext db = new DataProjectDataContext();

        [AllowAnonymous]
        public ActionResult Admin()
        {
            return View();
        }

        // POST: AdminPage
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AdminPage(Administrador admin)
        {
            connectionString();
            con.Open();
            com.Connection = con;
            com.CommandText = "Select * from adminTable where userAdmin='" + admin.Username + "' and passAdmin='" + admin.Password + "'";
            dr = com.ExecuteReader();
            if (dr.Read())
            {
                con.Close();
                
            }
            else
            {
                con.Close();
                return View("");
            }
            List<reporteSistema> reporte = db.reporteSistemas.ToList();
            return View(reporte);
        }

        void connectionString()
        {
            con.ConnectionString = "Server=BLX-PE-wmisari;Database=SigUNAC;User Id=sa;Password = Peru1234.; ";
        }

        // GET: LoginAccount
        [AllowAnonymous]
        public ActionResult Login(LoginAccount model)
        {
            if (ModelState.IsValid)
            {
                user eNuevo = new user();
                // eNuevo.IDnew = model.Id;
                eNuevo.nombreApellido = model.nNombre;
                eNuevo.correoElectronico = model.nCorreo;
                eNuevo.numeroCelular = model.nCelular;

                db.users.InsertOnSubmit(eNuevo);
                db.SubmitChanges();
                return RedirectToAction("Dashboard");
            }

            return View();
        }

        // GET: Dashboard
        [AllowAnonymous]
        public ActionResult Dashboard()
        {
            return View();
        }

        // GET: AboutUs
        [AllowAnonymous]
        public ActionResult AboutUs()
        {
            return View();
        }
   
        // GET: GTTIFilters
        [AllowAnonymous]
        public ActionResult GTTIFilters(CascadingClass model)
        {
            ViewBag.Nombre_Linea_Inv = new SelectList(db.lineaInvestigacions, "IDLinea_Investigacion", "Nombre_Linea_Inv");
            ViewBag.nameVerbo = new SelectList(db.verbos, "ID_Vbo", "nameVerbo");
            //ViewBag.nameProposito = new SelectList(db.propositos, "IDproposito", "nameProposito");
            ViewBag.nameNivel = new SelectList(db.niveles, "IDnivel", "nameNivel");
            ViewBag.simbolos = new SelectList(db.simbolos, "Idsimbolo", "nameSimbolo");

            if (ModelState.IsValid)
            {
                newElemento eNuevo = new newElemento();
                eNuevo.nEscuela = model.nEscuela;
                eNuevo.nCarrera = model.nCarrera;
                eNuevo.nArea = model.nArea;
                eNuevo.nNivel = model.nNivel;
                eNuevo.nProposito = model.nProposito;
                eNuevo.nVerbo = model.nVerbo;

                db.newElementos.InsertOnSubmit(eNuevo);
                db.SubmitChanges();
                return RedirectToAction("GTTIFilters");
            }

                reporteSistema reporte = new reporteSistema();

            if (model.titulote == null && model.espec == null && model.secu == null && model.hipote == null)
            {
                model.titulote = "";
                model.espec = "";
                model.secu = "";
                model.hipote = "";
            }

            if (model.titulote != null && model.titulote != "") {
                reporte.titulo = model.titulote;
                reporte.oEspecifico = model.espec;
                reporte.oSecundario = model.secu;
                reporte.hipotesisDeduccion = model.hipote;
                db.reporteSistemas.InsertOnSubmit(reporte);
                db.SubmitChanges();
                return RedirectToAction("Dashboard");
            }
           

            return View();
        }

        public IList<nivele> GetNivelList()
        {
            List<nivele> niveleses = db.niveles.ToList();
            return niveleses;
        }

        public ActionResult GetVerboList(int IDnivel)
        {
            List<verbo> selectLista = db.verbos.Where(x => x.IDnivel == IDnivel).ToList();
            ViewBag.verboList = new SelectList(selectLista, "ID_Vbo", "nameVerbo");
            return PartialView("DisplayVerbo");
        }

        public ActionResult GetVerboListado(int IDnivel)
        {
            List<verbo1> selectListado = db.verbo1s.Where(x => x.IDnivel == IDnivel).ToList();
            ViewBag.verboListado = new SelectList(selectListado, "ID_Vbo", "nameVerbo");
            return PartialView("DisplayAgainVerbo");
        }

        public ActionResult GetPropositoList(int IDnivel)
        {
            List<proposito> selectListProp = db.propositos.Where(x => x.IDnivel == IDnivel).ToList();
            ViewBag.propositoList = new SelectList(selectListProp, "IDproposito", "nameProposito");
            return PartialView("DisplayProposito");
        }

        public IList<lineaInvestigacion> GetLineaInvestigacionList() {
            List<lineaInvestigacion> lineaInvestigacions = db.lineaInvestigacions.ToList();
            return lineaInvestigacions;
        }

        public ActionResult GetCarreraList(int IDLinea_Investigacion)
        {
            List<carrera> selectList = db.carreras.Where(x => x.IDLinea_Investigacion == IDLinea_Investigacion).ToList();
            ViewBag.carreraList = new SelectList(selectList, "IDcarrera", "nameCarrera");
            return PartialView("DisplayCarrera");
        }

        public ActionResult GetAreaList(int IDcarrera)
        {
            List<areaInvestigacion> selectList = db.areaInvestigacions.Where(x => x.IDcarrera == IDcarrera).ToList();
            ViewBag.areaList = new SelectList(selectList, "IDarea", "nameArea");
            return PartialView("DisplayArea");
        }
    }
}