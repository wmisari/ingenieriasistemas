﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace proyectosig.Models
{
    public class LoginAccount
    {
        public string UserData { get; set; }
        public string Mail { get; set; }
        public string Cellphone { get; set; }

        [Required]
        [Display(Name = "nombre y Apellido")]
        [StringLength(200)]
        public string nNombre { get; set; }
        [Required]
        [Display(Name = "correo Electronico")]
        [StringLength(200)]
        public string nCorreo { get; set; }
        [Required]
        [Display(Name = "numero Celular")]
        [StringLength(200)]
        public string nCelular { get; set; }

    }

    public class CascadingClass
    {
        public int IDLinea_Investigacion { get; set; }
        public int IDcarrera { get; set; }
        public int IDarea { get; set; }
        public int IDnivel { get; set; }
        public int ID_Vbo { get; set; }
        public int IDVbo { get; set; }
        public int IDproposito { get; set; }

        public int Id { get; set; }
        [Required]
        [Display(Name = "Nivel")]
        [StringLength(200)]
        public string nNivel { get; set; }
        [Required]
        [Display(Name = "Escuela")]
        [StringLength(200)]
        public string nEscuela { get; set; }
        [Required]
        [Display(Name = "Carrera")]
        [StringLength(200)]
        public string nCarrera { get; set; }
        [Required]
        [Display(Name = "Area")]
        [StringLength(200)]
        public string nArea { get; set; }
        [Required]
        [Display(Name = "Proposito")]
        [StringLength(200)]
        public string nProposito { get; set; }
        [Required]
        [Display(Name = "Verbo")]
        [StringLength(200)]
        public string nVerbo { get; set; }

        public string titulote { get; set; }
        public string espec { get; set; }
        public string secu { get; set; }
        public string hipote { get; set; }
    }

    public class NewElement
    {
        public string nNivel { get; set; }
        public string nEscuela { get; set; }
        public string nCarrera { get; set; }
        public string nArea { get; set; }
    }

    public class Administrador
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

}
